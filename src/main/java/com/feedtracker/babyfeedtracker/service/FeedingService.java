package com.feedtracker.babyfeedtracker.service;

import com.feedtracker.babyfeedtracker.model.Feeding;
import org.springframework.stereotype.Service;

import java.util.List;


public interface FeedingService {
    public Feeding saveFeeding(Feeding feeding);
    public List<Feeding> getAllFeedings();

}
