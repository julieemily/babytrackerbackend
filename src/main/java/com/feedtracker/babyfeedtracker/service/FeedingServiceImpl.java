package com.feedtracker.babyfeedtracker.service;

import com.feedtracker.babyfeedtracker.model.Feeding;
import com.feedtracker.babyfeedtracker.repository.FeedingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedingServiceImpl implements FeedingService{

    @Autowired
    private FeedingRepository feedingRepository;

    @Override
    public Feeding saveFeeding(Feeding feeding) {
        return feedingRepository.save(feeding);
    }

    @Override
    public List<Feeding> getAllFeedings() {
        return feedingRepository.findAll();
    }
}
