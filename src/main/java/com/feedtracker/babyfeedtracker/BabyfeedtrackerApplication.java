package com.feedtracker.babyfeedtracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BabyfeedtrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BabyfeedtrackerApplication.class, args);
	}

}
