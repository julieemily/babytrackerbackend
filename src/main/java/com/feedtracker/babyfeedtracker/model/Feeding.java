package com.feedtracker.babyfeedtracker.model;

import com.feedtracker.babyfeedtracker.service.FeedingService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Array;
import java.util.List;


@Entity
public class Feeding {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String date;
    private String time;
    private int duration;
    private String breast;
    private String notes;
    private String pee;
    private String poop;

    public Feeding() {
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getBreast() {
        return breast;
    }

    public void setBreast(String breast) {
        this.breast = breast;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPee() {
        return pee;
    }

    public void setPee(String pee) {
        this.pee = pee;
    }

    public String getPoop() {
        return poop;
    }

    public void setPoop(String poop) {
        this.poop = poop;
    }


}
