package com.feedtracker.babyfeedtracker.repository;


import com.feedtracker.babyfeedtracker.model.Feeding;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedingRepository extends JpaRepository<Feeding, Integer> {
}
