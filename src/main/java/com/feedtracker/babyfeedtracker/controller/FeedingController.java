package com.feedtracker.babyfeedtracker.controller;

import com.feedtracker.babyfeedtracker.model.Feeding;
import com.feedtracker.babyfeedtracker.service.FeedingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/feeding")
public class FeedingController {

    @Autowired
    private FeedingService feedingService;

    @PostMapping("/add")
    @CrossOrigin(origins = "http://localhost:3000")
    public String add(@RequestBody Feeding feeding){
        feedingService.saveFeeding(feeding);
        return "New feeding is added";
    }

    @GetMapping("/getAll")
    @CrossOrigin(origins = "http://localhost:3000")
    public List<Feeding> getAllFeedings(){
        return feedingService.getAllFeedings();
    }

}
